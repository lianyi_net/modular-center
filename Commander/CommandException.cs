﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commander
{
    /// <summary>
    /// 命令异常
    /// </summary>
    public class CommandException : Exception
    {
        /// <summary>
        /// 命令异常
        /// </summary>
        /// <param name="message"></param>
        public CommandException(string message) : base(message) { }

        /// <summary>
        /// 命令异常
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innnerException"></param>
        public CommandException(string message, Exception innnerException) : base(message, innnerException) { }
    }
}
