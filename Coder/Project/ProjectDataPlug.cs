﻿using Coder.Configs;
using Egg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace Coder.Project
{
    public class ProjectDataPlug : IDataPlug
    {
        // 获取下划线命名法
        private static string GetUnderScoreCase(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                char chr = str[i];
                if (chr >= 'A' && chr <= 'Z')
                {
                    if (i == 0)
                    {
                        sb.Append(chr.ToString().ToLower());
                    }
                    else
                    {
                        sb.Append('_');
                        sb.Append(chr.ToString().ToLower());
                    }
                }
                else
                {
                    sb.Append(chr);
                }
            }
            return sb.ToString();
        }

        // 获取缩写
        private static string GetAbbr(string str)
        {
            if (str.IsEmpty()) return "";
            StringBuilder sb = new StringBuilder();
            sb.Append(str[0].ToString().ToLower());
            for (int i = 1; i < str.Length; i++)
            {
                char chr = str[i];
                if (chr >= 'A' && chr <= 'Z')
                {
                    sb.Append(chr.ToString().ToLower());
                }
            }
            return sb.ToString();
        }

        // 获取缩写
        private static string GetTuofeng(string str)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(str[0].ToString().ToLower());
            if (str.Length > 1) sb.Append(str.Substring(1));
            return sb.ToString();
        }

        public void Execute(ModuleConfig config, string name)
        {
            var data = config.Data;
            var dataProject = data[name];
            // 创建对象
            if (dataProject is null)
            {
                dataProject = new ModuleDataGroup();
                data[name] = dataProject;
                return;
            }
            // 处理变量
            string dataProjectName = dataProject["Name"];
            dataProject["UnderScoreCaseName"] = GetUnderScoreCase(dataProjectName);
            dataProject["Abbr"] = GetAbbr(dataProjectName);
            dataProject["TuofengName"] = GetTuofeng(dataProjectName);
        }
    }
}
