﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coder
{
    public static class GuidOpreator
    {
        private static Dictionary<string, string> guids;

        static GuidOpreator()
        {
            guids = new Dictionary<string, string>();
        }

        /// <summary>
        /// 获取内容
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetContent(string key)
        {
            if (!guids.ContainsKey(key))
            {
                guids[key] = Guid.NewGuid().ToString();
            }
            return guids[key];
        }
    }
}
