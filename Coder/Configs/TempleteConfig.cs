﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coder.Configs
{
    public class TempleteConfig : Dictionary<string, TempleteGroup>
    {
    }
    public class TempleteGroup
    {
        public string ModulePath { get; set; }
        public string EntityPath { get; set; }
    }
}
