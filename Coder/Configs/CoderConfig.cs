﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coder.Configs
{
    /// <summary>
    /// 代码助手定义
    /// </summary>
    public class CoderConfig
    {
        public CoderType Type { get; set; }
        public string NameSpace { get; set; }
        public bool Overwrite { get; set; }

        public CoderConfig()
        {
            this.Type = CoderType.None;
            this.NameSpace = "";
            this.Overwrite = false;
        }
    }
    public enum CoderType
    {
        None = 0,
        Module = 1,
        Entity = 2,
    }
}
