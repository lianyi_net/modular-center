﻿using Coder.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    public interface IDataPlug
    {
        void Execute(ModuleConfig config, string name);
    }
}
