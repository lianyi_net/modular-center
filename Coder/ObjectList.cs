﻿using Egg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Coder
{
    public class ObjectList : Dictionary<string, object>
    {
        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public T? GetValue<T>(params string[] keys)
        {
            foreach (var key in keys)
            {
                if (ContainsKey(key))
                {
                    var value = this[key];
                    if (value is JsonElement)
                    {
                        JsonElement jsonElement = (JsonElement)value;
                        string tpName = typeof(T).GetTopName();
                        switch (tpName)
                        {
                            case "System.String":
                                return (T?)(object?)jsonElement.GetString();
                            case "System.Byte":
                                return (T)(object)jsonElement.GetByte();
                            case "System.Int16":
                                return (T)(object)jsonElement.GetInt16();
                            case "System.Int32":
                                return (T)(object)jsonElement.GetInt32();
                            case "System.Int64":
                                return (T)(object)jsonElement.GetInt64();
                            case "System.UInt16":
                                return (T)(object)jsonElement.GetUInt16();
                            case "System.UInt32":
                                return (T)(object)jsonElement.GetUInt32();
                            case "System.UInt64":
                                return (T)(object)jsonElement.GetUInt64();
                            case "System.Single":
                                return (T)(object)jsonElement.GetSingle();
                            case "System.Double":
                                return (T)(object)jsonElement.GetDouble();
                            case "System.Decimal":
                                return (T)(object)jsonElement.GetDecimal();
                            case "System.Boolean":
                                return (T)(object)jsonElement.GetBoolean();
                            case "System.DateTime":
                                return (T)(object)jsonElement.GetDateTime();
                            default:
                                throw new Exception($"不支持的类型'{tpName}'");
                        }
                    }
                    return (T)value;
                }
            }
            return default;
        }
    }
}
