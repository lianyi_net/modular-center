﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    /// <summary>
    /// 可输出对象
    /// </summary>
    public interface IOutput
    {
        /// <summary>
        /// 换行输出
        /// </summary>
        /// <param name="content"></param>
        void WriteLine(string content);
        /// <summary>
        /// 输出
        /// </summary>
        /// <param name="content"></param>
        void Write(string content);
    }
}
