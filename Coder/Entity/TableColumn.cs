﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coder.Entity
{
    /// <summary>
    /// 表字段
    /// </summary>
    public class TableColumnProperties : ObjectList
    {

        /// <summary>
        /// 变量名称
        /// </summary>
        public string VarName => GetValue<string>("ColumnName", "VarName") ?? "";
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DbName => GetValue<string>("ColumnDbName", "DbName") ?? "";
        /// <summary>
        /// 显示文本
        /// </summary>
        public string Text => GetValue<string>("ColumnText", "Text") ?? "";
        /// <summary>
        /// 变量类型
        /// </summary>
        public virtual string VarType => GetValue<string>("ColumnVarType", "VarType") ?? "";
        /// <summary>
        /// 数据类型
        /// </summary>
        public string DbType => GetValue<string>("ColumnType", "DbType") ?? "";
        /// <summary>
        /// 类型长度
        /// </summary>
        public int DbTypeLength => GetValue<int>("ColumnTypeLength", "DbTypeLength");
        /// <summary>
        /// 任务精度
        /// </summary>
        public int DbTypeFloat => GetValue<int>("ColumnTypeFloat", "DbTypeFloat");
        /// <summary>
        /// 是否为主键
        /// </summary>
        public bool IsPrimaryKey => GetValue<bool>("ColumnPrimaryKey", "IsPrimaryKey");
        /// <summary>
        /// 是否可为空
        /// </summary>
        public bool IsNullable => GetValue<bool>("ColumnNullable", "IsNullable");
        /// <summary>
        /// 是否为必填
        /// </summary>
        public bool IsRequired => GetValue<bool>("ColumnRequired", "IsRequired");
        /// <summary>
        /// 是否需要创建
        /// </summary>
        public bool IsBuild => GetValue<bool>("ColumnMake", "IsBuild");
        /// <summary>
        /// 是否为索引
        /// </summary>
        public bool IsIndexer => GetValue<bool>("ColumnIndexer", "IsIndexer");
        /// <summary>
        /// 是否为唯一索引，需要搭配索引使用
        /// </summary>
        public bool IsUnique => GetValue<bool>("ColumnUnique", "IsUnique");

        /// <summary>
        /// 设置默认值
        /// </summary>
        public void SetDefaultValues()
        {
            this["VarName"] = "";
            this["DbName"] = "";
            this["Text"] = "";
            this["VarType"] = "";
            this["DbType"] = "";
            this["DbTypeLength"] = 0;
            this["DbTypeFloat"] = 0;
            this["IsPrimaryKey"] = false;
            this["IsNullable"] = false;
            this["IsRequired"] = false;
            this["IsBuild"] = false;
            this["IsIndexer"] = false;
            this["IsUnique"] = false;
        }
    }
}
