﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coder.Entity
{
    public class TableInfo
    {
        public virtual string TableSchema { get; set; }
        public virtual string TableName { get; set; }
        public virtual string TableAbbr { get; set; }
        public virtual string TableDescription { get; set; }
        public virtual string TableClassName { get; set; }
        public virtual string TableClassNamespace { get; set; }
        public virtual string TableInherit { get; set; }
        public virtual List<ColumnInfo> Columns { get; set; }
    }

    public class ColumnInfo
    {
        public virtual string ColumnName { get; set; }
        public virtual string ColumnPropertyName { get; set; }
        public virtual string ColumnDescription { get; set; }
        public virtual string ColumnType { get; set; }
        public virtual string ColumnPropertyType { get; set; }
        public virtual int ColumnTypeLength { get; set; }
        public virtual int ColumnTypeFloat { get; set; }
        public virtual bool ColumnNullable { get; set; }
        public virtual bool ColumnPrimaryKey { get; set; }
        public virtual bool ColumnRequired { get; set; }
    }
}
