﻿using Coder.Configs;
using Egg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;

namespace Coder.Entity
{
    /// <summary>
    /// 表
    /// </summary>
    public class Table
    {
        /// <summary>
        /// 所有属性
        /// </summary>
        public TableProperties Properties { get; set; } = new TableProperties();

        /// <summary>
        /// 所有字段
        /// </summary>
        public List<TableColumnProperties> Columns { get; set; } = new List<TableColumnProperties>();

        /// <summary>
        /// 获取句柄
        /// </summary>
        public long Handle { get; private set; }

        /// <summary>
        /// 设置句柄
        /// </summary>
        /// <param name="handle"></param>
        public void SetHandle(long handle) => Handle = handle;

        /// <summary>
        /// 创建一个新的属性
        /// </summary>
        /// <returns></returns>
        public static Table Create()
        {
            Table table = new Table();
            table.Properties.SetDefaultValues();
            TableColumnProperties tableColumnProperties = new TableColumnProperties();
            tableColumnProperties.SetDefaultValues();
            table.Columns.Add(tableColumnProperties);
            return table;
        }

        /// <summary>
        /// 从文件加载
        /// </summary>
        /// <param name="path"></param>
        /// <param name="isCreate"></param>
        /// <returns></returns>
        public static Table LoadFromFile(string path, bool isCreate = true)
        {
            // 生成默认配置文件
            if (!egg.IO.CheckFileExists(path))
            {
                if (isCreate)
                {
                    // 获取文件夹
                    string folder = System.IO.Path.GetDirectoryName(path) ?? "";
                    egg.IO.CreateFolder(folder);
                    // 创建一个新的配置文件
                    var table = Create();
                    // 保存文件
                    var content = JsonSerializer.Serialize(table);
                    egg.IO.WriteUtf8FileContent(path, content);
                    return table;
                }
                else
                {
                    return Create();
                }
            }
            else
            {
                // 从文件获取
                var content = egg.IO.ReadUtf8FileContent(path);
                var table = JsonSerializer.Deserialize<Table>(content);
                return table ?? Create();
            }
        }
    }

    /// <summary>
    /// 属性集合
    /// </summary>
    public class TableProperties : ObjectList
    {

        /// <summary>
        /// Abbr
        /// </summary>
        public virtual string Abbr => this.GetValue<string>("Abbr") ?? "";

        /// <summary>
        /// Schema
        /// </summary>
        public virtual string Schema => this.GetValue<string>("Schema") ?? "";

        /// <summary>
        /// 继承类
        /// </summary>
        public virtual string Inherit => this.GetValue<string>("Inherit") ?? "";

        /// <summary>
        /// 名称
        /// </summary>
        public virtual string VarName => this.GetValue<string>("Name", "VarName") ?? "";

        /// <summary>
        /// 数据库名称
        /// </summary>
        public virtual string DbName => this.GetValue<string>("DbName") ?? "";

        /// <summary>
        /// 显示文本
        /// </summary>
        public virtual string Text => this.GetValue<string>("Text") ?? "";

        /// <summary>
        /// 设置默认值
        /// </summary>
        public void SetDefaultValues()
        {
            this["Schema"] = "";
            this["Inherit"] = "";
            this["Name"] = "";
            this["DbName"] = "";
            this["Text"] = "";
        }

    }
}
