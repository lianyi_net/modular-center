﻿using Coder.Configs;
using Coder.Entity;
using Egg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModularCenter
{
    public partial class FrmCoderCore : Form
    {
        private readonly FrmParent _frmParent;
        private readonly string _folder;
        private ModuleConfig? _config;
        private string? _path;

        public FrmCoderCore(
            FrmParent frmParent,
            string folder)
        {
            _frmParent = frmParent;
            _folder = folder;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (DigConfigs dig = new DigConfigs(_frmParent))
            {
                dig.SourcePaths.Add("entity");
                dig.SourcePaths.Add("cache");
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    _config = dig.ModuleConfig ?? new ModuleConfig();
                    _path = dig.Path ?? "";
                    _path = _path.Replace('\\', '/');
                    if (_path.StartsWith("/")) _path = _path.Substring(1);
                    this.textBox1.Text = $"[{_config.IO.SourcePath}]{_config.Data[Config.CONFIG_PROJECT]["Name"]}({_config.Data[Config.CONFIG_PROJECT]["CNName"]})";
                    this.textBox3.Text = _path;
                }
            }
        }

        private void FrmCoderEntity_Load(object sender, EventArgs e)
        {
            this.textBox2.Text = _folder.Substring(_frmParent.WorkFolder.Length);
            this.txtNameSpace.Text = System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(_folder));
            this.txtTargetPath.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var nameSpace = this.txtNameSpace.Text.Trim();
                var targetPath = this.txtTargetPath.Text.Trim();
                if (_config is null) throw new Exception("请先选择一个实例");
                if (nameSpace.IsEmpty()) throw new Exception("请先填写命名空间");
                //if (targetPath.IsEmpty()) throw new Exception("请先填写目标目录");
                // 组织保存地址
                var cfgSource = _config.IO.SourcePath;
                var cfgProject = _config.Data[Config.CONFIG_PROJECT];
                var cfgEntity = _config.Data[Config.CONFIG_ENTITY];
                string name = cfgProject["Name"];
                string savePath = _folder + name + ".Core.json";
                if (egg.IO.CheckFileExists(savePath)) throw new Exception("文件已经存在");
                ModuleConfig module = ModuleConfig.CreateProjectModule();
                // 添加输出文件
                module.Enable = true;
                module.IO.SourcePath = "core";
                module.IO.TargetPath = targetPath;
                module.IO.Folders.Add("$(Project.Name)Core");
                module.IO.Folders.Add("$(Project.Name)Core/Sto");
                module.IO.Folders.Add("$(Project.Name)Core/Dto");
                // 产生基础模块
                module.Availabilities["Core"] = chkEntity.Checked;
                ModuleConfig.AddModuleIOFiles(module, "Core", new List<string>()
                {
                    "$(Project.Name)Core/$(Project.Name)Core.cs",
                    "$(Project.Name)Core/I$(Project.Name)Core.cs",
                    "$(Project.Name)Core/Sto/$(Project.Name)Input.cs",
                    "$(Project.Name)Core/Dto/$(Project.Name)Output.cs",
                    "$(Project.Name)Core/Dto/$(Project.Name)Dto.cs",
                });
                // 产生基础模块
                module.Availabilities["NoEntityCore"] = !chkEntity.Checked;
                ModuleConfig.AddModuleIOFile(module, "NoEntityCore", "$(Project.Name)Core/$(Project.Name)Core.NoEntity.cs", "$(Project.Name)Core/$(Project.Name)Core.cs");
                ModuleConfig.AddModuleIOFile(module, "NoEntityCore", "$(Project.Name)Core/I$(Project.Name)Core.NoEntity.cs", "$(Project.Name)Core/I$(Project.Name)Core.cs");
                ModuleConfig.AddModuleIOFiles(module, "NoEntityCore", new List<string>()
                {
                    "$(Project.Name)Core/Sto/$(Project.Name)Input.cs",
                    "$(Project.Name)Core/Dto/$(Project.Name)Output.cs",
                    "$(Project.Name)Core/Dto/$(Project.Name)Dto.cs",
                });
                // 基础业务模块
                module.Availabilities["Base"] = chkBase.Checked;
                ModuleConfig.AddModuleIOFiles(module, "Base", new List<string>()
                {
                    "$(Project.Name)Core/I$(Project.Name)Core.Query.cs",
                    "$(Project.Name)Core/$(Project.Name)Core.Query.cs",
                    "$(Project.Name)Core/Sto/$(Project.Name)EntitiesInput.cs",
                    "$(Project.Name)Core/Sto/$(Project.Name)InsertInput.cs",
                    "$(Project.Name)Core/Sto/$(Project.Name)UpdateInput.cs",
                });
                // 单例模块
                module.Availabilities["One"] = chkOne.Checked;
                ModuleConfig.AddModuleIOFiles(module, "One", new List<string>()
                {
                    "$(Project.Name)Core/$(Project.Name)Core.One.cs",
                    "$(Project.Name)Core/I$(Project.Name)Core.One.cs",
                    "$(Project.Name)Core/Sto/$(Project.Name)OneInput.cs",
                });
                // 下拉模块
                module.Availabilities["Dropdown"] = chkDropdown.Checked;
                ModuleConfig.AddModuleIOFiles(module, "Dropdown", new List<string>()
                {
                    "$(Project.Name)Core/$(Project.Name)Core.Dropdown.cs",
                    "$(Project.Name)Core/I$(Project.Name)Core.Dropdown.cs",
                    "$(Project.Name)Core/Sto/$(Project.Name)DropdownInput.cs",
                    "$(Project.Name)Core/Dto/$(Project.Name)DropdownOutput.cs",
                });
                // 导入模块
                module.Availabilities["Import"] = chkImport.Checked;
                ModuleConfig.AddModuleIOFiles(module, "Import", new List<string>()
                {
                    "$(Project.Name)Core/$(Project.Name)Core.Import.cs",
                    "$(Project.Name)Core/I$(Project.Name)Core.Import.cs",
                    "$(Project.Name)Core/Sto/$(Project.Name)ImportInput.cs",
                });
                // 添加插件
                module.DataPlug.Add("Project", "Coder.Project.ProjectDataPlug");
                module.DataPlug.Add("Entity", "Coder.Entity.EntityDataPlug");
                // 添加数据
                var data = module.Data;
                // 添加项目数据
                var dataProject = data[Config.CONFIG_PROJECT];
                dataProject["Schema"] = cfgProject["Schema"];
                dataProject["Name"] = cfgProject["Name"];
                dataProject["CNName"] = cfgProject["CNName"];
                switch (cfgSource)
                {
                    case "entity":
                        dataProject["EntitiesNameSpace"] = cfgProject["NameSpace"];
                        break;
                    case "cache":
                        dataProject["EntitiesNameSpace"] = cfgProject["EntitiesNameSpace"];
                        dataProject["CachesNameSpace"] = cfgProject["NameSpace"];
                        break;
                    default:
                        throw new Exception($"不支持的配置来源类型'{cfgSource}'");
                }
                dataProject["NameSpace"] = nameSpace;
                // 添加实例数据
                var dataEntity = data[Config.CONFIG_ENTITY];
                dataEntity["DefinePath"] = cfgEntity["DefinePath"];
                // 保存文件
                module.SaveToFile(savePath);
                // 返回结果
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, egg.Assembly.Name);
            }
        }
    }
}
