﻿using Coder.Configs;
using Coder.Entity;
using Egg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace ModularCenter
{
    public partial class DigConfigs : Form
    {
        private readonly List<ModuleConfig> _configs;
        private readonly List<string> _paths;
        private readonly FrmParent _frmParent;

        /// <summary>
        /// 表定义
        /// </summary>
        public ModuleConfig? ModuleConfig { get; private set; }

        /// <summary>
        /// 路径
        /// </summary>
        public string? Path { get; private set; }

        /// <summary>
        /// 来源类型
        /// </summary>
        public List<string> SourcePaths { get; }

        /// <summary>
        /// 实例选择窗口
        /// </summary>
        /// <param name="frmParent"></param>
        public DigConfigs(FrmParent frmParent)
        {
            this.SourcePaths = new List<string>();
            _configs = new List<ModuleConfig>();
            _paths = new List<string>();
            _frmParent = frmParent;
            InitializeComponent();
        }

        // 加载配置
        private void LoadConfigs(string path, string? rootPath = null)
        {
            if (rootPath is null) rootPath = path;
            var files = egg.IO.GetFiles(path, "*.json");
            foreach (var file in files)
            {
                string absPath = file.Substring(rootPath.Length);
                _paths.Add(absPath);
                _configs.Add(ModuleConfig.LoadFromFile(file, false));
            }
            var dirs = egg.IO.GetFolders(path);
            foreach (var dir in dirs)
            {
                LoadConfigs(dir, rootPath);
            }
        }

        private void Reload()
        {
            // 重新获取所有配置
            _configs.Clear();
            this.LoadConfigs(egg.IO.GetClosedPath(_frmParent.WorkFolder) + Config.DIR_CONFIGS);
            // 刷新列表
            this.listView1.Items.Clear();
            for (int i = 0; i < _configs.Count; i++)
            {
                var cfg = _configs[i];
                var path = _paths[i];
                string sourcePath = "";
                if (cfg.IO != null)
                    if (!cfg.IO.SourcePath.IsEmpty()) sourcePath = cfg.IO.SourcePath;
                // 只显示对应来源
                if (!this.SourcePaths.Contains(sourcePath)) continue;
                ListViewItem item = new ListViewItem(sourcePath);
                //item.SubItems.Add(table.Properties.VarName);
                if (cfg.Data is null)
                {
                    item.SubItems.Add("");
                    item.SubItems.Add("");
                }
                else
                {
                    item.SubItems.Add(cfg.Data[Config.CONFIG_PROJECT]["Name"]);
                    item.SubItems.Add(cfg.Data[Config.CONFIG_PROJECT]["CNName"]);
                }
                item.SubItems.Add(path);
                item.SubItems.Add(i.ToString());
                item.ImageIndex = 0;
                this.listView1.Items.Add(item);
            }
        }

        private void FrmEntity_Load(object sender, EventArgs e)
        {
            this.Text = $"选择实例";
            this.Reload();
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.listView1.SelectedIndices.Count <= 0) return;
            int index = this.listView1.SelectedIndices[0];
            var item = this.listView1.Items[index];
            var cfgIndex = item.SubItems[4].Text.ToInteger();
            this.ModuleConfig = _configs[cfgIndex];
            this.Path = _paths[cfgIndex];
            this.DialogResult = DialogResult.OK;
        }
    }
}
