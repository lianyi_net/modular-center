﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace ModularCenter.Dto
{
    /// <summary>
    /// 设置
    /// </summary>
    public class Setting
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; } = "";

        /// <summary>
        /// 项目路径
        /// </summary>
        public string ProjectPath { get; set; } = "";

        /// <summary>
        /// 工具集合
        /// </summary>
        public ToolSetting Tools { get; set; } = new ToolSetting();

        /// <summary>
        /// 从文件加载配置
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Setting LoadFromFile(string path)
        {
            if (!egg.IO.CheckFileExists(path))
            {
                Setting setting = new Setting();
                setting.Tools["EntityDesigner"] = "";
                setting.Tools["Coder"] = "";
                setting.Tools["VSCode"] = "";
                egg.IO.WriteUtf8FileContent(path, JsonSerializer.Serialize(setting));
                return setting;
            }
            else
            {
                string json = egg.IO.ReadUtf8FileContent(path);
                return JsonSerializer.Deserialize<Setting>(json) ?? new Setting();
            }
        }

        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="path"></param>
        public void SaveToFile(string path)
        {
            string json = JsonSerializer.Serialize(this);
            egg.IO.WriteUtf8FileContent(path, json);
        }
    }

    /// <summary>
    /// 工具设置
    /// </summary>
    public class ToolSetting : Dictionary<string, string>
    {
        /// <summary>
        /// 实例编辑器
        /// </summary>
        internal string EntityDesigner => this.ContainsKey("EntityDesigner") ? this["EntityDesigner"] : "";
        /// <summary>
        /// 代码生成器
        /// </summary>
        internal string Coder => this.ContainsKey("Coder") ? this["Coder"] : "";
        /// <summary>
        /// VSCode
        /// </summary>
        internal string VSCode => this.ContainsKey("VSCode") ? this["VSCode"] : "";
    }

}
