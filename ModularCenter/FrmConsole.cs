﻿using Commander;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModularCenter
{
    public partial class FrmConsole : Form
    {
        //private Terminal _terminal;
        //private IntPtr _terminalWindowHandle;
        private bool _working;
        private readonly string _folder;

        public FrmConsole(string folder)
        {
            _folder = folder;
            InitializeComponent();
        }

        /// <summary>
        /// 释放资源
        /// </summary>
        public new void Dispose()
        {
            _working = false;
            this.Close();
            base.Dispose();
        }

        private void FrmTerminal_Load(object sender, EventArgs e)
        {
            _working = true;
            //_terminal = new Terminal(_folder);
            this.textBox1.Left = 0;
            this.textBox1.Top = 0;
            this.textBox1.Width = this.DisplayRectangle.Width;
            this.textBox1.Height = this.DisplayRectangle.Height;
            //this.timer1.Start();
        }

        /// <summary>
        /// 清屏
        /// </summary>
        /// <param name="content"></param>
        public void Clear()
        {
            this.textBox1.Text = "";
        }

        /// <summary>
        /// 输出
        /// </summary>
        /// <param name="content"></param>
        public void Write(string content)
        {
            this.textBox1.AppendText(content);
        }

        /// <summary>
        /// 输出换行
        /// </summary>
        /// <param name="content"></param>
        public void WriteLine(string content)
        {
            this.textBox1.AppendText(content);
            this.textBox1.AppendText("\r\n");
        }

        //// 重新绘制
        //private void Repaint()
        //{

        //}

        private void timer1_Tick(object sender, EventArgs e)
        {
            //this.timer1.Stop();
            //int lineHeight = 18;
            ////int allHeight = _terminal.Ouputs.Count * lineHeight + 10;
            ////int top = this.panel1.Height - allHeight;
            //if (top > 0) top = 0;
            //Font font = new Font("Consolas", 9f);
            //if (this.WindowState != FormWindowState.Minimized)
            //{
            //    using (Graphics eg = this.panel1.CreateGraphics())
            //    {
            //        using (Bitmap bitmap = new Bitmap(this.panel1.Width, this.panel1.Height, eg))
            //        {
            //            using (Graphics g = Graphics.FromImage(bitmap))
            //            {
            //                g.Clear(Color.Black);
            //                for (int i = 0; i < _terminal.Ouputs.Count; i++)
            //                {
            //                    float y = 5 + i * lineHeight + top;
            //                    if (y + lineHeight > 0)
            //                        g.DrawString(_terminal.Ouputs[i], font, Brushes.White, new PointF(5, y));
            //                }
            //            }
            //            eg.DrawImageUnscaled(bitmap, 0, 0);
            //        }
            //    }
            //}
            //this.timer1.Start();
        }

        private void FrmTerminal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_working)
            {
                this.Visible = false;
                e.Cancel = true;
            }
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //_terminal.RunCommand(this.textBox1.Text);
                //this.textBox1.Text = "";
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            //this.Refresh();
        }
    }
}
