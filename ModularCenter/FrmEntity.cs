﻿using Coder.Entity;
using Egg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModularCenter
{
    public partial class FrmEntity : Form
    {
        private readonly string _folder;

        public FrmEntity(string folder)
        {
            _folder = folder;
            InitializeComponent();
        }

        private void Reload()
        {
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "entities" + _folder;
            var files = egg.IO.GetFiles(folder, "*.json");
            this.listView1.Items.Clear();
            foreach (var file in files)
            {
                string name = System.IO.Path.GetFileName(file);
                var info = new FileInfo(file);
                var config = Table.LoadFromFile(file);
                ListViewItem item = new ListViewItem(name);
                item.SubItems.Add(config.Properties.Text);
                item.SubItems.Add(egg.Time.Parse(info.LastWriteTime).ToDateTimeString());
                item.ImageIndex = 0;
                this.listView1.Items.Add(item);
            }
        }

        private void FrmEntity_Load(object sender, EventArgs e)
        {
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "entities" + _folder;
            egg.IO.CreateFolder(folder);
            this.Text = $"实例管理 - [{_folder}]";
            this.Reload();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            this.Reload();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "entities" + _folder;
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = frmParent.Setting.Tools.EntityDesigner,
                WorkingDirectory = folder,
            };
            Process.Start(startInfo);
        }

        private void EditFile()
        {
            if (this.listView1.SelectedItems.Count <= 0) return;
            var item = this.listView1.SelectedItems[0];
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "entities" + _folder;
            var path = folder + item.Text;
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = frmParent.Setting.Tools.EntityDesigner,
                WorkingDirectory = folder,
                Arguments = $"\"{path}\"",
            };
            Process.Start(startInfo);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            this.EditFile();
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.EditFile();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count <= 0) return;
            var item = this.listView1.SelectedItems[0];
            if (MessageBox.Show($"确定要删除 {item.Text} 吗?", egg.Assembly.Name, MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                FrmParent frmParent = (FrmParent)this.MdiParent;
                var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "entities" + _folder;
                var path = folder + item.Text;
                egg.IO.DeleteFile(path);
                this.Reload();
            }
        }
    }
}
