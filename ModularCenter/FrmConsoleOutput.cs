﻿using Coder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModularCenter
{
    public class FrmConsoleOutput : IOutput
    {
        private delegate void OutputText(object sender, string content);

        private static object _lock = new object();

        private readonly FrmConsole _frmConsole;

        private void SetOutput(object sender, string content)
        {
            lock (_lock)
            {
                FrmConsole console = (FrmConsole)sender;
                //if (!this.textBox1.Text.IsEmpty()) this.textBox1.AppendText("\r\n");
                //this.textBox1.AppendText(content);\
                console.Activate();
                console.WriteLine(content);
            }
        }

        public FrmConsoleOutput(FrmConsole frmConsole) { _frmConsole = frmConsole; }

        public void Write(string content)
        {
            //_frmConsole.Write(content);
            _frmConsole.Invoke(new OutputText(SetOutput), new object[] { _frmConsole, content });
        }

        public void WriteLine(string content)
        {
            Write(content + "\r\n");
        }
    }
}
