﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModularCenter
{
    internal static class Config
    {
        /// <summary>
        /// 实例配置文件夹
        /// </summary>
        public const string DIR_ENTITIES = "entities";
        /// <summary>
        /// 模板代码文件夹
        /// </summary>
        public const string DIR_CODES = "codes";
        /// <summary>
        /// 配置文件夹
        /// </summary>
        public const string DIR_CONFIGS = "configs";
        /// <summary>
        /// 项目配置
        /// </summary>
        public const string CONFIG_PROJECT = "Project";
        /// <summary>
        /// 实例配置
        /// </summary>
        public const string CONFIG_ENTITY = "Entity";
    }
}
