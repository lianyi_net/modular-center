﻿namespace ModularCenter
{
    partial class FrmCoderCore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCoderCore));
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNameSpace = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTargetPath = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkBase = new System.Windows.Forms.CheckBox();
            this.chkOne = new System.Windows.Forms.CheckBox();
            this.chkDropdown = new System.Windows.Forms.CheckBox();
            this.chkImport = new System.Windows.Forms.CheckBox();
            this.chkEntity = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "前置配置";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(92, 77);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(220, 23);
            this.textBox1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(312, 76);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(61, 25);
            this.button1.TabIndex = 2;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "生成目录";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(92, 37);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(465, 23);
            this.textBox2.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "命名空间";
            // 
            // txtNameSpace
            // 
            this.txtNameSpace.Location = new System.Drawing.Point(92, 157);
            this.txtNameSpace.Name = "txtNameSpace";
            this.txtNameSpace.Size = new System.Drawing.Size(220, 23);
            this.txtNameSpace.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "生成目录";
            // 
            // txtTargetPath
            // 
            this.txtTargetPath.Location = new System.Drawing.Point(92, 197);
            this.txtTargetPath.Name = "txtTargetPath";
            this.txtTargetPath.Size = new System.Drawing.Size(281, 23);
            this.txtTargetPath.TabIndex = 8;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(437, 296);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 32);
            this.button2.TabIndex = 9;
            this.button2.Text = "保存";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "配置路径";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(92, 117);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(281, 23);
            this.textBox3.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "生成模块";
            // 
            // chkBase
            // 
            this.chkBase.AutoSize = true;
            this.chkBase.Checked = true;
            this.chkBase.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBase.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBase.Location = new System.Drawing.Point(173, 239);
            this.chkBase.Name = "chkBase";
            this.chkBase.Size = new System.Drawing.Size(75, 21);
            this.chkBase.TabIndex = 13;
            this.chkBase.Text = "基础业务";
            this.chkBase.UseVisualStyleBackColor = true;
            // 
            // chkOne
            // 
            this.chkOne.AutoSize = true;
            this.chkOne.Location = new System.Drawing.Point(254, 239);
            this.chkOne.Name = "chkOne";
            this.chkOne.Size = new System.Drawing.Size(75, 21);
            this.chkOne.TabIndex = 14;
            this.chkOne.Text = "单例模块";
            this.chkOne.UseVisualStyleBackColor = true;
            // 
            // chkDropdown
            // 
            this.chkDropdown.AutoSize = true;
            this.chkDropdown.Location = new System.Drawing.Point(335, 239);
            this.chkDropdown.Name = "chkDropdown";
            this.chkDropdown.Size = new System.Drawing.Size(75, 21);
            this.chkDropdown.TabIndex = 15;
            this.chkDropdown.Text = "下拉模块";
            this.chkDropdown.UseVisualStyleBackColor = true;
            // 
            // chkImport
            // 
            this.chkImport.AutoSize = true;
            this.chkImport.Location = new System.Drawing.Point(416, 238);
            this.chkImport.Name = "chkImport";
            this.chkImport.Size = new System.Drawing.Size(75, 21);
            this.chkImport.TabIndex = 16;
            this.chkImport.Text = "导入模块";
            this.chkImport.UseVisualStyleBackColor = true;
            // 
            // chkEntity
            // 
            this.chkEntity.AutoSize = true;
            this.chkEntity.Checked = true;
            this.chkEntity.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEntity.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkEntity.Location = new System.Drawing.Point(92, 239);
            this.chkEntity.Name = "chkEntity";
            this.chkEntity.Size = new System.Drawing.Size(75, 21);
            this.chkEntity.TabIndex = 17;
            this.chkEntity.Text = "拥有实例";
            this.chkEntity.UseVisualStyleBackColor = true;
            // 
            // FrmCoderCore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 373);
            this.Controls.Add(this.chkEntity);
            this.Controls.Add(this.chkImport);
            this.Controls.Add(this.chkDropdown);
            this.Controls.Add(this.chkOne);
            this.Controls.Add(this.chkBase);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtTargetPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNameSpace);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmCoderCore";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "新建 Cache 配置";
            this.Load += new System.EventHandler(this.FrmCoderEntity_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private TextBox textBox1;
        private Button button1;
        private Label label2;
        private TextBox textBox2;
        private Label label3;
        private TextBox txtNameSpace;
        private Label label4;
        private TextBox txtTargetPath;
        private Button button2;
        private Label label5;
        private TextBox textBox3;
        private Label label6;
        private CheckBox chkBase;
        private CheckBox chkOne;
        private CheckBox chkDropdown;
        private CheckBox chkImport;
        private CheckBox chkEntity;
    }
}