﻿using Coder.Configs;
using Coder.Entity;
using Egg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModularCenter
{
    public partial class FrmCoderEntity : Form
    {
        private readonly FrmParent _frmParent;
        private readonly string _folder;
        private Table? _table;
        private string? _path;

        public FrmCoderEntity(
            FrmParent frmParent,
            string folder)
        {
            _frmParent = frmParent;
            _folder = folder;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (DigEntities dig = new DigEntities(_frmParent))
            {
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    _table = dig.Table ?? new Table();
                    _path = dig.Path ?? "";
                    _path = _path.Replace('\\', '/');
                    if (_path.StartsWith("/")) _path = _path.Substring(1);
                    this.textBox1.Text = $"{_table.Properties.Text}({_table.Properties.VarName})";
                    this.textBox3.Text = _path;
                }
            }
        }

        private void FrmCoderEntity_Load(object sender, EventArgs e)
        {
            this.textBox2.Text = _folder.Substring(_frmParent.WorkFolder.Length);
            this.txtNameSpace.Text = System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(_folder));
            this.txtTargetPath.Text = "Entities/";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var nameSpace = this.txtNameSpace.Text.Trim();
                var targetPath = this.txtTargetPath.Text.Trim();
                if (_table is null) throw new Exception("请先选择一个实例");
                if (nameSpace.IsEmpty()) throw new Exception("请先填写命名空间");
                //if (targetPath.IsEmpty()) throw new Exception("请先填写目标目录");
                // 组织保存地址
                string savePath = _folder + _table.Properties.VarName + ".Entity.json";
                if (egg.IO.CheckFileExists(savePath)) throw new Exception("文件已经存在");
                ModuleConfig module = ModuleConfig.CreateProjectModule();
                // 添加输出文件
                module.Enable = true;
                module.IO.SourcePath = "entity";
                module.IO.TargetPath = targetPath;
                module.Availabilities["Core"] = true;
                ModuleConfig.AddModuleIOFiles(module, "Core", new List<string>() {
                    "$(Project.Name).cs",
                });
                // 添加插件
                module.DataPlug.Add("Project", "Coder.Project.ProjectDataPlug");
                module.DataPlug.Add("Entity", "Coder.Entity.EntityDataPlug");
                // 添加数据
                var data = module.Data;
                // 添加项目数据
                var dataProject = data[Config.CONFIG_PROJECT];
                dataProject["Schema"] = _table.Properties.Schema;
                dataProject["Name"] = _table.Properties.VarName;
                dataProject["CNName"] = _table.Properties.Text;
                dataProject["NameSpace"] = nameSpace;
                // 添加实例数据
                var dataEntity = data[Config.CONFIG_ENTITY];
                dataEntity["DefinePath"] = _path ?? "";
                // 保存文件
                module.SaveToFile(savePath);
                // 返回结果
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, egg.Assembly.Name);
            }
        }
    }
}
