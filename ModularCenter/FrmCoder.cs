﻿using Coder.Configs;
using Egg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModularCenter
{
    public partial class FrmCoder : Form
    {
        private readonly string _folder;

        public FrmCoder(string folder)
        {
            _folder = folder;
            InitializeComponent();
        }

        private void Reload()
        {
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + Config.DIR_CONFIGS + _folder;
            var files = egg.IO.GetFiles(folder, "*.json");
            this.listView1.Items.Clear();
            foreach (var file in files)
            {
                string name = System.IO.Path.GetFileName(file);
                var info = new FileInfo(file);
                var config = ModuleConfig.LoadFromFile(file);
                string text = "";
                if (config.Data != null)
                    if (config.Data.ContainsKey("Project"))
                        text = config.Data["Project"]["CNName"];
                ListViewItem item = new ListViewItem(name);
                item.SubItems.Add(text);
                item.SubItems.Add(egg.Time.Parse(info.LastWriteTime).ToDateTimeString());
                item.ImageIndex = 0;
                this.listView1.Items.Add(item);
            }
        }

        private void FrmEntity_Load(object sender, EventArgs e)
        {
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "configs" + _folder;
            egg.IO.CreateFolder(folder);
            this.Text = $"生成管理 - [{_folder}]";
            this.Reload();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            this.Reload();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "configs" + _folder;
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = frmParent.Setting.Tools.EntityDesigner,
                WorkingDirectory = folder,
            };
            Process.Start(startInfo);
        }

        private void EditFile()
        {
            if (this.listView1.SelectedItems.Count <= 0) return;
            var item = this.listView1.SelectedItems[0];
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "configs" + _folder;
            var path = folder + item.Text;
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = frmParent.Setting.Tools.EntityDesigner,
                WorkingDirectory = folder,
                Arguments = $"\"{path}\"",
            };
            Process.Start(startInfo);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            //this.EditFile();
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "configs" + _folder;
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = frmParent.Setting.Tools.VSCode,
                Arguments = $"\"{folder}\"",
            };
            Process.Start(startInfo);
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.EditFile();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count <= 0) return;
            var item = this.listView1.SelectedItems[0];
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "configs" + _folder;
            var codesFolder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "codes";
            var path = folder + item.Text;
            try
            {
                Coder.ModuleMaker.MakeCodeFromPath(new Coder.Configs.CoderConfig()
                {
                    Overwrite = false,
                }, egg.IO.GetClosedPath(frmParent.WorkFolder), path, codesFolder, frmParent.Setting.ProjectPath + _folder);
            }
            catch (Exception ex)
            {
                frmParent.Console.WriteLine(ex.ToString());
            }
        }

        private void toolStripButton3_Click_1(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count <= 0) return;
            var item = this.listView1.SelectedItems[0];
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + Config.DIR_CONFIGS + _folder;
            var codesFolder = egg.IO.GetClosedPath(frmParent.WorkFolder) + Config.DIR_CODES;
            var path = folder + item.Text;
            try
            {
                Coder.ModuleMaker.MakeCodeFromPath(new Coder.Configs.CoderConfig()
                {
                    Overwrite = true,
                }, egg.IO.GetClosedPath(frmParent.WorkFolder), path, codesFolder, frmParent.Setting.ProjectPath + _folder);
            }
            catch (Exception ex)
            {
                frmParent.Console.WriteLine(ex.ToString());
            }
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + Config.DIR_CONFIGS + _folder;
            using (FrmCoderEntity f = new FrmCoderEntity(frmParent, folder))
            {
                f.ShowDialog();
                this.Reload();
            }
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count <= 0) return;
            var item = this.listView1.SelectedItems[0];
            if (MessageBox.Show($"确定要删除 {item.Text} 吗?", egg.Assembly.Name, MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                FrmParent frmParent = (FrmParent)this.MdiParent;
                var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + Config.DIR_CONFIGS + _folder;
                var path = folder + item.Text;
                egg.IO.DeleteFile(path);
                this.Reload();
            }
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + Config.DIR_CONFIGS + _folder;
            using (FrmCoderCache f = new FrmCoderCache(frmParent, folder))
            {
                f.ShowDialog();
                this.Reload();
            }
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + Config.DIR_CONFIGS + _folder;
            using (FrmCoderCore f = new FrmCoderCore(frmParent, folder))
            {
                f.ShowDialog();
                this.Reload();
            }
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + Config.DIR_CONFIGS + _folder;
            using (FrmCoderApp f = new FrmCoderApp(frmParent, folder))
            {
                f.ShowDialog();
                this.Reload();
            }
        }
    }
}
