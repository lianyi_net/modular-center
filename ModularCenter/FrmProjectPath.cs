using Commander;
using Egg;
using Microsoft.VisualBasic.Devices;
using System.Diagnostics;
using System.IO;

namespace ModularCenter
{
    public partial class FrmProjectPath : Form
    {
        private readonly FrmParent _frmParent;
        private readonly string _folder;

        private TreeNode? _node = null;

        public FrmProjectPath(
            FrmParent frmParent,
            string folder)
        {
            _frmParent = frmParent;
            _folder = folder;
            InitializeComponent();
        }

        private void LoadPath(string path, TreeNode parentNode)
        {
            FrmParent frmParent = (FrmParent)this.MdiParent;
            var folder = frmParent.WorkFolder;
            var dirs = egg.IO.GetFolders(path);
            foreach (var dir in dirs)
            {
                string name = System.IO.Path.GetFileName(dir);
                if (!name.StartsWith("."))
                {
                    string nodeName = parentNode.Name + name + "/";
                    string[] entities = new string[0];
                    string[] configs = new string[0];
                    TreeNode node = new TreeNode();
                    node.Name = nodeName;
                    node.Text = name;
                    node.ImageIndex = 0;
                    node.SelectedImageIndex = 0;
                    node.ContextMenuStrip = this.contextMenuStrip1;
                    parentNode.Nodes.Add(node);
                    LoadPath(dir, node);
                }
            }
        }

        private void FrmProject_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 重新加载
        /// </summary>
        public void Reload()
        {
            //FrmParent frmParent = (FrmParent)this.MdiParent;
            //var folder = frmParent.WorkFolder;
            //var setting = frmParent.Setting;
            //// 清理列表和当前选择节点
            //this.treeView1.Nodes.Clear();
            //_node = null;
            //TreeNode nodeRoot = new TreeNode();
            //nodeRoot.Text = "项目";
            //nodeRoot.Name = "/";
            //this.treeView1.Nodes.Add(nodeRoot);
            //if (!setting.ProjectPath.IsEmpty())
            //{
            //    nodeRoot.Text = $"项目[{setting.ProjectName}]";
            //    LoadPath(setting.ProjectPath, nodeRoot);
            //}
            //else
            //{
            //    string name = System.IO.Path.GetFileName(setting.ProjectPath);
            //    nodeRoot.Text = $"项目[{name}]";
            //}
            //nodeRoot.Expand();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //if (_node is null) return;
            //FrmEntity frmEntity = new FrmEntity(_node.Name);
            //frmEntity.MdiParent = this.MdiParent;
            //frmEntity.Show();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            _node = e.Node;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.Reload();
        }

        private void toolMakeEntity_Click(object sender, EventArgs e)
        {
            if (_node is null) return;
            FrmParent frmParent = (FrmParent)this.MdiParent;
            //var folder = egg.IO.GetClosedPath(frmParent.WorkFolder) + "configures";
            //var path = folder + _node.Name;
            //ProcessStartInfo startInfo = new ProcessStartInfo()
            //{
            //    FileName = frmParent.Setting.Tools.Coder,
            //    //WorkingDirectory = folder,
            //    Arguments = $"--source:\"{frmParent.WorkFolder}\" --target:\"{frmParent.Setting.ProjectPath}\" --config:\"{_node.Name}\"",
            //};
            //Process.Start(startInfo);
            string arg = $"--source:\"{frmParent.WorkFolder.Substring(0, frmParent.WorkFolder.Length - 1)}\" --target:\"{frmParent.Setting.ProjectPath}\" --config:\"{_node.Name}\"";
            using (Runner runner = new Runner(frmParent.Setting.Tools.Coder, arg))
            {
                //SetOutput(frmParent.Console, runner.Command);
                runner.OutputLine += Runner_OutputLine;
                runner.ErrorLine += Runner_OutputLine;
                runner.Run();
            }
        }

        private void Runner_OutputLine(object sender, CommandOutputLineEventArgs e)
        {
            //throw new NotImplementedException();
            //FrmParent frmParent = (FrmParent)this.MdiParent;
            //frmParent.Console.Invoke(new OutputText(SetOutput), new object[] { frmParent.Console, e.Content });
        }

        private void toolCoder_Click(object sender, EventArgs e)
        {
            if (_node is null) return;
            FrmCoder frmCoder = new FrmCoder(_node.Name);
            frmCoder.MdiParent = this.MdiParent;
            frmCoder.Show();
        }
    }
}